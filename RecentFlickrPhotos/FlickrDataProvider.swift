//
//  FlickrDataProvider.swift
//  RecentFlickrPhotos
//
//  Created by Nora Madjarova on 2.04.18.
//  Copyright © 2018 Nora Madjarova. All rights reserved.
//

import Foundation

class FlickrDataProvider {
    
    //errors returned from Flickr API
    struct FlickrErrors {
        //error message example:
        //{"stat":"fail","code":100,"message":"Invalid API Key (Key has invalid format)"}
        static let accessProblemErrorCode = 100
    }
    
    private static let flickrKey = "d0ea18911e9c473748528c5ee59b5da8"
    private static let flickrRecentURL = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=\(flickrKey)&per_page=20&format=json&nojsoncallback=1")
    
    /**
     Fetches recent photos from Flickr service.
     */
    class func fetchRecentPhotos(onCompletion: @escaping (Error?, [FlickrPhoto]?) -> Void) -> Void {
        
        //make sure the URL is valid and not nil, otherwise abort call
        guard let fetchURL = flickrRecentURL else {
            print("No valid URL provided");
            
            return
        }
        
        
        let fetchTask = URLSession.shared.dataTask(with: fetchURL, completionHandler: {data, response, error -> Void in
            
            if error != nil {
                print("Error fetching photos: \(String(describing: error))")
                onCompletion(error as NSError?, nil)
                
                return
            }
            
            do {
                let resultsDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                
                guard let results = resultsDictionary else {
                    return
                }
                
                if let statusCode = results["code"] as? Int, statusCode == FlickrErrors.accessProblemErrorCode {
                    
                    let invalidAccessError = NSError(domain: "com.flickr.api", code: statusCode, userInfo: nil)
                    onCompletion(invalidAccessError, nil)
                    
                    return
                    
                }
                
                guard let photosContainer = results["photos"] as? NSDictionary, let photosArray = photosContainer["photo"] as? [NSDictionary] else {
                    
                    return
                }
                
                var flickrPhotosArray : [FlickrPhoto] = Array()
                
                for photoData in photosArray {
                    guard let photoDictionary = photoData as? [String : Any] else {
                        continue
                    }
                    
                    do {
                        let flickrPhoto = try FlickrPhoto(json: photoDictionary)
                        flickrPhotosArray.append(flickrPhoto)
                        
                    } catch {
                        print("Problem with Serialization")
                    }
                }
                
                onCompletion(nil, flickrPhotosArray)
                
            } catch let error as NSError {
                print("Error parsing JSON: \(error)")
                onCompletion(error, nil)
                
                return
            }
            
        })
        
        fetchTask.resume()
        
    }
    
    /**
     Downloads an image from URL
     - Parameter url - The URL from which to download the image.
     */
    class func downloadImageFromURL(_ url: URL?, onCompletion: @escaping (Error?, Data?) -> Void) -> Void {
        guard let imageURL = url else {
            print("No valid URL provided!")
            
            return
        }
        
        let session = URLSession(configuration: .default)
        
        let getImageFromUrl = session.dataTask(with: imageURL) { (data, response, error) in
            
            if let e = error {
                //print the error message
                print("Error : \(e) while downloading the image from url: \(imageURL)")
                
            } else {
                //in case of no error, make sure the response is not nil
                if (response as? HTTPURLResponse) != nil {
                    
                    if let imageData = data {
                        
                        onCompletion(error, imageData)
                        
                    } else {
                        print("Image file is currupted")
                    }
                } else {
                    print("No response from server")
                }
            }
        }
        
        getImageFromUrl.resume()
    }
}
