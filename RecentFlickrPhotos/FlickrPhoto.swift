//
//  FlickrPhoto.swift
//  RecentFlickrPhotos
//
//  Created by Nora Madjarova on 2.04.18.
//  Copyright © 2018 Nora Madjarova. All rights reserved.
//

import Foundation

//photo data model
struct FlickrPhoto {
    
    let photoId: String
    let farm: Int
    let secret: String
    let server: String
    
    var photoUrl: URL? {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoId)_\(secret)_m.jpg")
    }
    
}

enum SerializationError : Error {
    case missing(String)
}

//mapping JSON data to model
extension FlickrPhoto {
    
    init (json: [String : Any]) throws {
        
        //extract photoId
        guard let photoId = json["id"] as? String else {
            throw SerializationError.missing("photoId")
        }
        
        //extract farm
        guard let farm = json["farm"] as? Int else {
            throw SerializationError.missing("farm")
        }
        
        //extract secret
        guard let secret = json["secret"] as? String else {
            throw SerializationError.missing("secret")
        }
        
        //extract server
        guard let server = json["server"] as? String else {
            throw SerializationError.missing("server")
        }
        
        self.photoId = photoId
        self.farm = farm
        self.secret = secret
        self.server = server

    }
}
