//
//  ViewController.swift
//  RecentFlickrPhotos
//
//  Created by Nora Madjarova on 2.04.18.
//  Copyright © 2018 Nora Madjarova. All rights reserved.
//

import UIKit

class ImageLoaderViewController: UIViewController {

    @IBOutlet weak var flickrImageView: UIImageView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var pageCounter = 0; //the current page we're at
    var photos: [FlickrPhoto] = [] //the recent photos returned from API, parsed as FlickrPhoto
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //add gesture recognizer for left swipe
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleLeftSwipe(recognizer:)))
        leftSwipe.direction = .left
        view.addGestureRecognizer(leftSwipe)
        
        FlickrDataProvider.fetchRecentPhotos(onCompletion: { [weak self] (error: Error?, flickrPhotos: [FlickrPhoto]?) in
            
            guard let weakSelf = self else {
                return
            }
            
            if let fetchingError = error {
                weakSelf.photos = []
                
                if (fetchingError._code == FlickrDataProvider.FlickrErrors.accessProblemErrorCode) {
                    DispatchQueue.main.async(execute: { () -> Void in
                        weakSelf.showErrorAlert(title: "API Error", message: "Invalid API Key")
                    })
                }

            } else if let photos = flickrPhotos {
                    weakSelf.photos = photos
            }
        })
    }
    
    /**
     Showing error popup
     - Parameter title: the title of the popup
     - Parameter message: the message of the popup
     */
    private func showErrorAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /**
     Handling left swipe event.
     - Parameter recognizer: The UISwipeGestureRecognizer sending the event.
     */
    @objc func handleLeftSwipe(recognizer: UISwipeGestureRecognizer?) -> Void {
        guard self.photos.count > 0 else {
            self.showErrorAlert(title: "API Error", message: "No images")
            
            return
        }
        
        loadingActivityIndicator.startAnimating()
        
        FlickrDataProvider.downloadImageFromURL(self.photos[pageCounter].photoUrl, onCompletion: { (error: Error?, imageData: Data?) -> Void in
            //get the image
            if let image = imageData {
                
                let imageFromData = UIImage(data: image)
                
                //display the image
                DispatchQueue.main.async { [weak self] in
                    guard let weakSelf = self else {
                        return
                    }
                    
                    weakSelf.loadingActivityIndicator.stopAnimating()
                    
                    weakSelf.flickrImageView.image = imageFromData
                }
            }
        })
        
        if (pageCounter < self.photos.count - 1) {
            pageCounter += 1
            
        } else {
            pageCounter = 0
        }
    }
}
